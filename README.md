Steps:
- Get this repo and go into the vagrant directory
-- git clone git@bitbucket.org:allied_dev/allied-setup.git allied-setup && cd allied-setup/vagrant
- Run vagrant up
- Run vagrant ssh
- sudo to root and check out /var/www for stuff

Now you have a Ubuntu 14.10 web development box with Apache 2.4, PHP 5.5 (mysqlnd, gd, memcached, xdebug), MySQL 5.5 and more!

Note that the www-data user has a ssh key linked to the Allied Dev account on BitBucket to check out code
Use sudo -H -u www-data git clone git clone git@bitbucket.org:allied_dev/some-repo.git to get code