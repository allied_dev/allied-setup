#!/bin/bash

echo "Running setup from `pwd` "
puppet module install puppetlabs-stdlib
puppet module install puppetlabs-apache
puppet module install puppetlabs-apt
puppet module install puppetlabs-concat
puppet module install puppetlabs-inifile
puppet module install puppetlabs-mysql
puppet module install puppetlabs-ntp
puppet module install nodes-php
puppet module install puppetlabs-vcsrepo
echo "Changing to /var/www"
cd /var/www
echo "Changing to puppet/etc/puppet"
cd puppet/etc/puppet
echo "Running puppet apply manifests/ubuntu-web-server.pp"
puppet apply manifests/ubuntu-web-server.pp
