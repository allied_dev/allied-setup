#
# Manifest for Ubuntu Web Server for Allied WordPress sites
#

/**
  @todo Break out the apache vhost stuff for RDBMS settings
*/

node allied-wp {

  package { 'git': ensure => installed }
  package { 'curl': ensure => installed }
  package { 'npm': ensure => installed }
  package { 'htop': ensure => installed }
  package { 'vim': ensure => installed }
  package { 'memcached': ensure => installed }

  class { 'apache':
    mpm_module => 'prefork'
  }
  #class {'::apache::mod::php': }
  class { 'php::apache': }
  class { 'php::extension::curl': }
  class { 'php::extension::gd': }
  class { 'php::extension::intl': }
  class { 'php::extension::http': package => 'php-http-request2' }
  class { 'php::extension::mcrypt': }
  class { 'php::extension::memcache': }
  class { 'php::extension::mysql': }
  #class { 'php::extension::mysqlnd': }
  apache::mod { 'php': }
  apache::mod { 'rewrite': }
  apache::mod { 'status': }

}

node "allied-prod-web-1" inherits allied-wp {

  apache::vhost { '24x7mag.com':
    serveraliases => ["*.${title}"],
    port          => '80',
    docroot       => '/var/www/public',
    directories => [
      {
        path => '/var/www/public', # this is /var/www/wordpress/public on prod servers
        order => 'allow,deny',
        allow => 'from all',
        options => [
          '-Indexes',
          '+FollowSymLinks',
          '+MultiViews'
        ],
        allow_override => 'all',
        setenv => [
          'APP_ENV production',
          'APP_DB_HOST a360wp.chq7hovau8uh.us-west-2.rds.amazonaws.com',
          'APP_DB_NAME a360_wp_x7mag',
          'APP_DB_PASS SHGZr7ZzUa5DMn',
          'APP_UPLOADS_DIR x7mag',
          'HTTP_ENVIRONMENT "production"'
        ],
        custom_fragment => 'php_value newrelic.framework "wordpress"
		php_value newrelic.appname "PHP Application"'
      }
    ]
  }

}

node "allied-dev" inherits allied-wp {

  #
  # Apache and PHP stuff
  #

  apache::vhost { 'local.24x7mag.com':
    serveraliases => ["*.${title}"],
    port          => '80',
    docroot       => '/var/www/project/wordpress/public',
    directories => [
      {
        path => '/var/www/project/wordpress/public', # this is /var/www/wordpress/public on prod servers
        order => 'allow,deny',
        allow => 'from all',
        options => [
          '-Indexes',
          '+FollowSymLinks',
          '+MultiViews'
        ],
        allow_override => 'all',
        setenv => [
          'APP_ENV development',
          'APP_DB_HOST localhost',
          'APP_DB_NAME a360_wp_x7mag',
          'APP_DB_PASS a360',
          'APP_UPLOADS_DIR x7mag',
          'HTTP_ENVIRONMENT "development"'
        ],
      }
    ]
  }

  apache::vhost { 'local.sleepreviewmag.com':
    serveraliases => ["*.${title}"],
    port          => '80',
    docroot       => '/var/www/project/wordpress/public',
    directories => [
      {
        path => '/var/www/project/wordpress/public', # this is /var/www/wordpress/public on prod servers
        order => 'allow,deny',
        allow => 'from all',
        options => [
          '-Indexes',
          '+FollowSymLinks',
          '+MultiViews'
        ],
        allow_override => 'all',
        setenv => [
          'APP_ENV development',
          'APP_DB_HOST localhost',
          'APP_DB_NAME a360_wp_sleeprev',
          'APP_DB_PASS a360',
          'APP_UPLOADS_DIR sleeprev',
          'HTTP_ENVIRONMENT "development"'
        ],
      }
    ]
  }

  apache::vhost { 'trackr':
    serveraliases => ["*.${title}"],
    port          => '80',
    docroot       => '/var/www/project/trackr/public',
    directories => [
      {
        path => '/var/www/project/trackr/public', # this is /var/www/wordpress/public on prod servers
        order => 'allow,deny',
        allow => 'from all',
        options => [
          '-Indexes',
          '+FollowSymLinks',
          '+MultiViews'
        ],
        allow_override => 'all',
        setenv => [
          'APP_ENV development'
        ],
        rewrites => [
          {
            comment      => 'Laravel',
            rewrite_cond => ['%{REQUEST_FILENAME} !-d', '%{REQUEST_FILENAME} !-f'],
            rewrite_rule => ['^ index.php [L]'],
          },
        ],
      }
    ]
  }

  class { 'php::extension::xdebug': }

  #
  # MySQL stuff
  #

  class { '::mysql::server':
    root_password    => 'a360',
  }

  class { '::mysql::client': }

  mysql::db { 'a360_wp_x7mag':
    user     => 'x7mag',
    password => 'x7mag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_x7mag.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_clpmag':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_clpmag.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_hearingr':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_hearingr.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_imaginge':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_imaginge.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_orthodon':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_orthodon.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_plastics':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_plastics.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_ptproduc':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_ptproduc.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_rehabpub':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_rehabpub.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_rtmagazi':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_rtmagazi.sql',
    import_timeout => 900,
  }

  mysql::db { 'a360_wp_sleeprev':
    user     => 'clpmag',
    password => 'clpmag',
    host     => 'localhost',
    grant    => ['ALL'],
    sql      => '/var/www/puppet/sql_data/a360_wp_sleeprev.sql',
    import_timeout => 900,
  }

}
